//
//  FetchError.swift
//  SwiftVIPERArchitecture
//
//  Created by Bao Vu on 12/3/23.
//

import Foundation

enum FetchError: Error {
    case fail
}
