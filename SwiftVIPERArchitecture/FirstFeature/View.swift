//
//  View.swift
//  SwiftVIPERArchitecture
//
//  Created by Bao Vu on 12/3/23.
//

import Foundation
import UIKit

// ViewController
// Protocol
// Ref to presenter

protocol FirstFeatureView {
    var persenter: FirstFeaturePresenter? { get set }
    
    func update(with users: [User])
    
    func update(with error: String)
}

class UserViewController: UIViewController , FirstFeatureView {
    var persenter: FirstFeaturePresenter?
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.isHidden = true
        return table
    }()
    
    private let label: UILabel = {
       let label = UILabel()
        label.textAlignment = .center
        label.isHidden = true
        return label
    }()
    
    var users: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        label.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        label.center = view.center
    }
    
    func setUpUI()
    {
        view.backgroundColor = .red
        view.addSubview(tableView)
        view.addSubview(label)
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func update(with users: [User]) {
        DispatchQueue.main.async {
            self.users = users
            self.tableView.reloadData()
            self.tableView.isHidden = false
        }
        
    }
    
    func update(with error: String) {
        DispatchQueue.main.async {
            self.users = []
            self.tableView.isHidden = true
            self.label.isHidden = false
            self.label.text = error
        }
    }
    
    
}

extension UserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = users[indexPath.row].name
        return cell
    }
    
    
}
