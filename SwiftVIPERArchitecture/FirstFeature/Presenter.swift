//
//  Presenter.swift
//  SwiftVIPERArchitecture
//
//  Created by Bao Vu on 12/3/23.
//

import Foundation

// Object
// protocol
// ref to interactor, router, view



protocol FirstFeaturePresenter {
    var router: FristFeatureRouter? { get set }
    var interactor: FirstFeatureInteractor? { get set }
    var view: FirstFeatureView? { get set }
    
    func interactorDidFetchUsers(with result: Result<[User], Error>)
}

class UserPresenter: FirstFeaturePresenter {
    
    var router: FristFeatureRouter?
    
    var interactor: FirstFeatureInteractor? {
        didSet {
            interactor?.getUsers()
        }
    }
    
    var view: FirstFeatureView?

    
    func interactorDidFetchUsers(with result: Result<[User], Error>) {
        switch result {
        case .success(let users):
            view?.update(with: users)
        case .failure:
            view?.update(with: "Something went wrong")
        }
    }
    
}
