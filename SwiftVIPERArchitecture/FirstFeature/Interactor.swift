//
//  Interactor.swift
//  SwiftVIPERArchitecture
//
//  Created by Bao Vu on 12/3/23.
//

import Foundation

// Object
// Protocol
// Ref to presentor

// https://jsonplaceholder.typicode.com/users

protocol FirstFeatureInteractor {
    var presentor: FirstFeaturePresenter? { get set }
    
    func getUsers()
}

class UserInteractor: FirstFeatureInteractor {
    var presentor: FirstFeaturePresenter?
    
    func getUsers() {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/users") else { return }
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, res, err in
            
            guard let data = data, err == nil else {
                self?.presentor?.interactorDidFetchUsers(with: .failure(FetchError.fail))
                return
            }
            
            do {
                let entity = try JSONDecoder().decode([User].self, from: data)
                self?.presentor?.interactorDidFetchUsers(with: .success(entity))
            } catch {
                self?.presentor?.interactorDidFetchUsers(with: .failure(FetchError.fail))
            }
        }
        task.resume()
    }
    
    
}
