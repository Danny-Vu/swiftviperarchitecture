//
//  Router.swift
//  SwiftVIPERArchitecture
//
//  Created by Bao Vu on 12/3/23.
//

import Foundation
import UIKit

// Object
// Entry point

typealias EntryPoint = FirstFeatureView & UIViewController

protocol FristFeatureRouter {
    var entry: EntryPoint? { get set }
    
    static func start() -> FristFeatureRouter
}

class UserRouter: FristFeatureRouter {
    var entry: EntryPoint?
    
    static func start() -> FristFeatureRouter {
        let router = UserRouter()
        
        // Assign VIP
        var view: FirstFeatureView = UserViewController()
        var interator: FirstFeatureInteractor = UserInteractor()
        var presentor: FirstFeaturePresenter = UserPresenter()
        
        view.persenter = presentor
        interator.presentor = presentor
        presentor.view = view
        presentor.interactor = interator
        presentor.router =  router
        router.entry = view as? EntryPoint
        
        return router
    }
    
    
}
